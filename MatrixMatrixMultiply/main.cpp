// C++ program to multiply
// two square matrices.
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <chrono>
#include <map>
#include <omp.h>

#include <boost/numeric/odeint.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>


using namespace std;
using namespace std::chrono;
using namespace boost::numeric::odeint;

typedef boost::multiprecision::cpp_dec_float_50 dec50;

// This function multiplies
// mat1[][] and mat2[][], and
// stores the result in res[][]
template <typename T>
void multiply(
    const int n,
    T** mat1,
    T** mat2,
    T** res)
{

#pragma omp parallel for
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            res[i][j] = 0;
            for (int k = 0; k < n; k++)
                res[i][j] += mat1[i][k] * mat2[k][j];
        }
    }
}

template <typename T>
int driver(int argc, char* argv[]) {
    map<int, T> results;

    /* initialize random seed: */
    srand((unsigned int)time(NULL));

    const int nstart = atoi(argv[1]);
    const int noff = atoi(argv[2]);
    const int nmax = atoi(argv[3]);
    const int runs = atoi(argv[4]);

    omp_set_num_threads(atoi(argv[5]));

    for (int n = nstart; n < nmax; n += noff)
    {
        T temp = T(0);
        duration<double> time_span{ 0 };

        T** res = new T* [n];
        T** mat1 = new T* [n];
        T** mat2 = new T* [n];

        for (int i = 0; i < n; i++) {
            res[i] = new T[n];
            mat1[i] = new T[n];
            mat2[i] = new T[n];
        }

        // Different runs
        for (int r = 0; r < runs; r++) {

            // Fill with random values
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    res[i][j] = 0;
                    //mat1[i][j] = T{ rand() % 20000 - 10000 };
                    //mat2[i][j] = T{ rand() % 20000 - 10000 };

                    mat1[i][j] = mat2[i][j] = T( i * j - (((i * j) % 2) * 2 * i * j) );
                }
            }

            high_resolution_clock::time_point t1 = high_resolution_clock::now();
            multiply<T>(n, mat1, mat2, res);
            high_resolution_clock::time_point t2 = high_resolution_clock::now();

            duration<double> inter_time_span = duration_cast<duration<double>>(t2 - t1);
            time_span += inter_time_span;
            temp += res[n - 1][n - 1];
        }

        /*std::cout << "n: " << n << " took " << (time_span.count()/runs) <<
         " (" << time_span.count() << ")" << " secs. (" << temp << ")";
        std::cout << std::endl;*/

        std::cout << "( " << n << " ; " << time_span.count() / runs << " )" << endl;

        results[n] = time_span.count() / runs;

        for (int i = 0; i < n; i++) {
            delete[] res[i];
            delete[] mat1[i];
            delete[] mat2[i];
        }

        delete[] res;
        delete[] mat1;
        delete[] mat2;

    }

    for (auto it = results.begin(); it != results.end(); ++it) {
        cout << "( " << it->first << " ; " << it->second << " )" << endl;
    }

    /*cout << "Result matrix is \n";
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++)
            cout << res[i][j] << " ";
        cout << "\n";
    }*/

    return 0;
}


// Driver Code
int main(int argc, char* argv[])
{
    return driver<dec50>(argc, argv);
    //return driver<int>(argc, argv);
}

// This code is contributed
// by Soumik Mondal