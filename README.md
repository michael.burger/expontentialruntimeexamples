# ExpontentialRuntimeExamples

Generates runtime behavior of exponential runtime algorithms.

Choose 1 Test marked by comments //RUN X in the code.

`g++ -O2 ExpontialRuntimeTester\ExpontialRuntimeTester.cpp --std=c++17 -o ExpontialRuntimeTester`

call

`./ExpontialRuntimeTester DIM`

with `DIM` being the start dimension.
